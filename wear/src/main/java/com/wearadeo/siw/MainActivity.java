/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wearadeo.siw;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;
import com.mariux.teleport.lib.TeleportClient;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private GridViewPager pager;
    private DotsPageIndicator dotsPageIndicator;

    private List<com.wearadeo.siw.DeviceData> devices;

    private TeleportClient teleportClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pager = (GridViewPager) findViewById(R.id.pager);
        dotsPageIndicator = (DotsPageIndicator) findViewById(R.id.page_indicator);
        dotsPageIndicator.setPager(pager);

        teleportClient = new TeleportClient(this);
        teleportClient.setOnGetMessageTask(new MessageTask());
        teleportClient.setOnSyncDataItemTask(new SyncDataTask());
    }

    @Override
    protected void onStart() {
        super.onStart();
        teleportClient.connect();

        teleportClient.sendMessage("bonjour", null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        teleportClient.disconnect();
    }

    public void startMainScreen() {
        Log.d("PTP", "startMainScreen!");

        runOnUiThread(() -> {
            Log.d("PTP", "startMainScreen2!");

            findViewById(R.id.textView).setVisibility(View.INVISIBLE);

            if (pager != null && pager.getAdapter() == null) {
                pager.setAdapter(new SampleGridPagerAdapter(devices, MainActivity.this, getFragmentManager()));
            } else {
                Log.e("PTP", "Error startMainScreen!");
            }
        });
    }
    private class MessageTask extends TeleportClient.OnGetMessageTask {
        @Override
        protected void onPostExecute(String path) {
            if(SIWSyncCommons.KEY_SENSIT_DEVICES_CONFIG.equals(path)) {
                Log.d("PTP", "send configure!");

                teleportClient.sendMessage("configure", null);

                ((TextView)findViewById(R.id.textView)).setText("Configure API access token on your phone please!");

            }
            teleportClient.setOnGetMessageTask(new MessageTask());
        }
    }

    private class SyncDataTask extends TeleportClient.OnSyncDataItemTask {

        @Override
        protected void onPostExecute(DataMap dataMap) {
            Log.d("PTP", "dataMap:" + dataMap);

            devices = new ArrayList<>();

            if (dataMap.containsKey(SIWSyncCommons.KEY_SENSIT_DEVICES)) {

                List<DataMap> devicesDataMap = dataMap.getDataMapArrayList(SIWSyncCommons.KEY_SENSIT_DEVICES);

                Log.d("PTP", "devicesDataMap:" + devicesDataMap);

                for (DataMap device : devicesDataMap) {
                    String id = device.getString("id");
                    com.wearadeo.siw.DeviceData d = new com.wearadeo.siw.DeviceData(id);

                    List<DataMap> sensorsDataMap = device.getDataMapArrayList("sensors");

                    Log.d("PTP", "sensorsDataMap:" + sensorsDataMap);

                    List<com.wearadeo.siw.SensorData> sensors = new ArrayList<>();
                    for (DataMap sensor : sensorsDataMap) {
                        com.wearadeo.siw.SensorData s = new com.wearadeo.siw.SensorData();
                        s.id = sensor.getString("id");
                        s.sensor_type = sensor.getString("sensor_type");
                        s.value = sensor.getString("value");
                        sensors.add(s);
                    }

                    d.sensors = sensors.toArray(new com.wearadeo.siw.SensorData[0]);

                    devices.add(d);
                }

                startMainScreen();
            }

            teleportClient.setOnSyncDataItemTask(new SyncDataTask());
        }
    }
}
