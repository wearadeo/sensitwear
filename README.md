# Sens'it Wear

Permet de récupérer les informations collecter par les capteurs Sens'it utilisant le réseau Sigfox.

Pour chaque capteurs Sens'it associé à votre compte, les dernières valeurs relevées sont affichés sur votre montre.

Cette application a été réalisé pour prendre en main le développement Android Wear et pour tester le réseau Sigfox avec les capteurs Sens'it développés par Axible avec des composants Texas Instrument.

Cette première version est très (trop) simple alors merci de proposer des améliorations ;-)