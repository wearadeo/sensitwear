package com.wearadeo.siw;

public final class SIWSyncCommons {
    private SIWSyncCommons() {
        //no-op
    }

    public static final String PATH_SENSIT = "/com.wearadeo.sensit";
    public static final String KEY_SENSIT_DEVICES = "KEY_SENSIT_DEVICES";
    public static final String KEY_SENSIT_DEVICES_ERROR = "KEY_SENSIT_DEVICES_ERROR";
    public static final String KEY_SENSIT_DEVICES_CONFIG = "KEY_SENSIT_DEVICES_CONFIG";

}
