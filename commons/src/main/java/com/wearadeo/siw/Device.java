package com.wearadeo.siw;

public class Device {

    public int results;
    public DeviceData data;

    @Override
    public String toString() {
        return "Device{" +
                "results=" + results +
                ", data=" + data +
                '}';
    }
}
