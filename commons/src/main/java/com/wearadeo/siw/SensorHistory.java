package com.wearadeo.siw;

public class SensorHistory {

    public String data;
    public String date;
    public String date_period;

    @Override
    public String toString() {
        return "SensorHistory{" +
                "data='" + data + '\'' +
                ", date='" + date + '\'' +
                ", date_period='" + date_period + '\'' +
                '}';
    }
}
