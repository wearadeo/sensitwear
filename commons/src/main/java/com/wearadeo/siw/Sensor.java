package com.wearadeo.siw;

public class Sensor {

    public SensorData data;

    @Override
    public String toString() {
        return "Sensor{" +
                "data=" + data +
                '}';
    }
}
