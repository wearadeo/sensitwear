package com.wearadeo.siw;

import java.util.Arrays;

public class SensorData {

    public SensorHistory[] history;
    public String id;
    public String sensor_type;

    public String value;

    @Override
    public String toString() {
        return "SensorData{" +
                "history=" + Arrays.toString(history) +
                ", id='" + id + '\'' +
                ", sensor_type='" + sensor_type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
