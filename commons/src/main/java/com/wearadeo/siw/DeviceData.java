package com.wearadeo.siw;

import java.util.Arrays;

public class DeviceData {
    public DeviceData(String id) {
        this.id = id;
    }

    public DeviceData() {
    }

    public String id;
    public int mode;
    public SensorData[] sensors;

    @Override
    public String toString() {
        return "DeviceData{" +
                "id='" + id + '\'' +
                ", mode=" + mode +
                ", sensors=" + Arrays.toString(sensors) +
                '}';
    }
}
