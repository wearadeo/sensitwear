package com.wearadeo.siw;


import java.util.Arrays;

public class Devices {

    public int results;
    public DeviceData[] data;

    @Override
    public String toString() {
        return "Devices{" +
                "results=" + results +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
