package com.wearadeo.siw.api;


import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

public interface SensItApi {

    String ENDPOINT = "https://api.sensit.io/v1";

    @GET("/devices")
    Observable<com.wearadeo.siw.Devices> listDevices();

    @GET("/devices/{deviceId}")
    Observable<com.wearadeo.siw.Device> getDevice(@Path("deviceId") String deviceId);

    @GET("/devices/{deviceId}/sensors/{sensorId}")
    Observable<com.wearadeo.siw.Sensor> getSensor(@Path("deviceId") String deviceId, @Path("sensorId") String sensorId);
}
