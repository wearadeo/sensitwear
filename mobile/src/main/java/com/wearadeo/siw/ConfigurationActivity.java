package com.wearadeo.siw;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.EditText;
import android.widget.TextView;

import com.wearadeo.siw.service.WearService;

public class ConfigurationActivity extends AppCompatActivity {

    private ConfigPreferences configPreferences;

    private WearService wearService;
    boolean mServiceBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        findViewById(R.id.sensitBtn).setOnClickListener(v -> {
            saveToken();
        });

        ((TextView) findViewById(R.id.helpToken)).setMovementMethod(LinkMovementMethod.getInstance());

        configPreferences = ConfigPreferences.newInstance(this);

        ((EditText) findViewById(R.id.token)).setText(configPreferences.getToken());

        Intent intent = new Intent(this, WearService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void saveToken() {
        String token = ((EditText) findViewById(R.id.token)).getText().toString();

        configPreferences.setToken(token);

        wearService.sendDevices();
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            wearService = ((WearService.LocalBinder) service).getService();
            mServiceBound = true;
        }
    };
}