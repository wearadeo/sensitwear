package com.wearadeo.siw;

import android.content.Context;
import android.content.SharedPreferences;

public class ConfigPreferences {

    private static final String NAME = "ConfigPreferences";
    private static final String KEY_TOKEN = NAME + ".KEY_TOKEN";
    private static final String DEFAULT_TOKEN = "";

    private final SharedPreferences preferences;

    public static ConfigPreferences newInstance(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return new ConfigPreferences(preferences);
    }

    ConfigPreferences(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public String getToken() {
        return preferences.getString(KEY_TOKEN, DEFAULT_TOKEN);
    }

    public void setToken(String token) {
        preferences.edit().putString(KEY_TOKEN, token).apply();
    }

}