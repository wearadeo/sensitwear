package com.wearadeo.siw.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.mariux.teleport.lib.TeleportClient;
import com.wearadeo.siw.ConfigPreferences;
import com.wearadeo.siw.ConfigurationActivity;
import com.wearadeo.siw.SIWSyncCommons;
import com.wearadeo.siw.Sensor;
import com.wearadeo.siw.api.SensItApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WearService extends Service {

    private TeleportClient teleportClient;
    private SensItApi sensIt;
    private ConfigPreferences configPreferences;
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public WearService getService() {
            return WearService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        teleportClient = new TeleportClient(this);

        configPreferences = ConfigPreferences.newInstance(this);

        RequestInterceptor requestInterceptor = request -> request.addHeader("Authorization", "Bearer " + configPreferences.getToken());

        sensIt = new RestAdapter.Builder()
                .setEndpoint(SensItApi.ENDPOINT)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(msg -> Log.i("SENSIT", msg))
                .build()
                .create(SensItApi.class);

        teleportClient.connect();

        teleportClient.setOnGetMessageTask(new MessageTask());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        teleportClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void sendNoConfigCallToAction() {
        teleportClient.sendMessage(SIWSyncCommons.KEY_SENSIT_DEVICES_CONFIG, null);
    }

    private boolean checkConfig() {
        return !configPreferences.getToken().isEmpty();
    }

    public void sendDevices() {

        Log.d("PTP", "sendDevices");

        Observable<List<Pair<com.wearadeo.siw.Device, List<Sensor>>>> devicesObservable = sensIt.listDevices()
                .flatMap(devices -> Observable.from(Arrays.asList(devices.data)))
                .flatMap(deviceData -> sensIt.getDevice(deviceData.id))
                .flatMap((com.wearadeo.siw.Device device) -> {
                    Observable<List<Sensor>> sensorObs = Observable.from(Arrays.asList(device.data.sensors))
                            .flatMap(sensor -> sensIt.getSensor(device.data.id, sensor.id)).toList();
                    return Observable.zip(
                            Observable.just(device),
                            sensorObs,
                            (device1, sensors) -> new Pair<>(device1, sensors));
                })
                .toList();

        devicesObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(throwable -> new ArrayList<>())
                .subscribe(listDeviceSensors -> {

                    final PutDataMapRequest putDataMapReq = PutDataMapRequest.create(SIWSyncCommons.PATH_SENSIT);

                    final ArrayList<DataMap> devicesDataMap = new ArrayList<>();

                    if (listDeviceSensors.isEmpty()) {
                        teleportClient.syncBoolean(SIWSyncCommons.KEY_SENSIT_DEVICES_ERROR, true);
                    }

                    for (Pair<com.wearadeo.siw.Device, List<Sensor>> deviceListPair : listDeviceSensors) {
                        final DataMap deviceDataMap = new DataMap();
                        deviceDataMap.putString("id", deviceListPair.first.data.id);

                        final ArrayList<DataMap> sensorsDataMap = new ArrayList<>();
                        for (Sensor sensor : deviceListPair.second) {
                            final DataMap sensorDataMap = new DataMap();
                            sensorDataMap.putString("sensor_type", sensor.data.sensor_type);
                            sensorDataMap.putString("id", sensor.data.id);
                            String value = "N/A";
                            if (sensor.data != null
                                    && sensor.data.history != null
                                    && sensor.data.history.length > 0
                                    && sensor.data.history[0].data != null)
                                value = sensor.data.history[0].data.split(":")[0];
                            sensorDataMap.putString("value", value);
                            sensorsDataMap.add(sensorDataMap);
                        }

                        deviceDataMap.putDataMapArrayList("sensors", sensorsDataMap);
                        devicesDataMap.add(deviceDataMap);

                        Log.d("PTP", "devicesDataMap: " + devicesDataMap);
                    }

                    putDataMapReq.getDataMap().putDataMapArrayList(SIWSyncCommons.KEY_SENSIT_DEVICES, devicesDataMap);
                    putDataMapReq.getDataMap().putLong("timestamp", System.currentTimeMillis());

                    PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
                    Log.d("PTP", "putDataReq: " + putDataReq.toString());
                    teleportClient.syncDataItem(putDataMapReq);

                });

    }

    private class MessageTask extends TeleportClient.OnGetMessageTask {

        @Override
        protected void onPostExecute(String path) {
            Log.d("PTP", "onMessageReceived!");

            Log.d("PTP", "path:" + path);

            if (path.equals("bonjour")) {
                if (checkConfig()) {
                    sendDevices();
                } else {
                    sendNoConfigCallToAction();
                }
            } else if (path.equals("configure")) {
                Intent intent = new Intent(WearService.this, ConfigurationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                WearService.this.startActivity(intent);
            }

            teleportClient.setOnGetMessageTask(new MessageTask());
        }
    }

}
